/**
 * Created by AvS on 21.02.2017.
 */
$(document).ready(function () {
    $.scrollify({
        section: ".js-scroll",
        interstitialSection: "l-header",
        offset: 0,
        setHeights: false,
        easing: "easeOutExpo",
        touchScroll: true,
        scrollSpeed: 1200
    });
});
