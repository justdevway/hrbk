var flashlight = document.getElementById('blurLoupe'),
    light = {
        posX: 0,
        posY: 0,
        radius: 70
    };

flashlight.width = $('.l-team__blurBlock__teamImg').width();
flashlight.height = $('.l-team__blurBlock__teamImg').height();

flashlight.addEventListener('mousemove', function (evt) {
    setLightPos(evt);
    renderLight();
}, false);

window.addEventListener('resize', function () {
    renderLight();
}, false);

var setLightPos = function (evt) {
    var rect = flashlight.getBoundingClientRect();
    light.posX = evt.clientX - rect.left;
    light.posY = evt.clientY - rect.top;
};

var renderLight = function () {
    var ctx = flashlight.getContext('2d');
    ctx.save();
    ctx.clearRect(0, 0, flashlight.width, flashlight.height);
    ctx.fillStyle = 'rgba(0, 0, 0, .8)';
    ctx.fillRect(0, 0, flashlight.width, flashlight.height);
    ctx.globalCompositeOperation = 'destination-out';
    ctx.beginPath();
    ctx.arc(light.posX, light.posY, light.radius, 0, Math.PI * 2, false);
    ctx.fillStyle = 'white';
    ctx.shadowOffsetX = 1;
    ctx.shadowOffsetY = 1;
    ctx.shadowBlur = 70;
    ctx.shadowColor = 'rgba(255, 255, 255, 0.5)';
    ctx.fill();
    ctx.restore();
};


var mainBlock = document.getElementById("scrollForThisBlock")

document.onscroll = function (e) {
    var scrollTop = document.body.scrollTop;
    var elementTop = mainBlock.offsetTop;
    console.log(elementTop + "elementTop");
    console.log(scrollTop + "scrollTop");

    if ( (scrollTop + 60) >= elementTop) {
        console.log("++++")
        function addOnWheel(element, handler) {
            if (element.addEventListener) {
                if ('onwheel' in document) {
                    element.addEventListener("wheel", handler);
                } else if ('onmousewheel' in document) {
                    element.addEventListener("mousewheel", handler);
                } else {
                    element.addEventListener("MozMousePixelScroll", handler);
                }
            } else {
                text.attachEvent("onmousewheel", handler);
            }
        }

        var scaleX = 1;
        var scaleY = 1;
        var rotate = 0;
        var opacity = 1;
        var display = "block";
        var wheelOn = document.getElementById("hiddenBlock");
        var disabled = document.getElementById("hiddenBlock__content");
        var blurBg = document.getElementById("blurLoupe");
        var teamText = document.getElementById("hideThisText");

        var imgsIn = document.getElementById("imgThatIn");
        var blockIn = document.getElementById("blockThatIn");


        var scrollAndScale = addOnWheel(wheelOn, function (e) {

            var delta = e.deltaY || e.detail || e.wheelDelta;
            if (delta > 0) {
                scaleX -= 0.15;
                scaleY -= 0.1;
                teamText.style.opacity = 0;

            }
            else if (delta < 0 && scaleX <= 0.935) {
                scaleX += 0.01;
                scaleY += 0.05;
            }
            if (scaleX <= 0.6) {
                opacity -= 0.2;
            }

            else if (scaleX > 0.6 && delta < 0 && scaleX < 0.8) {
                opacity += 0.1;
            }
            if (opacity <= 0.2) {
                display = "none";
                imgsIn.style.opacity = 1;
                blockIn.style.opacity = 1;
            }
            if (opacity > 0.3) {
                display = "block";
            }
            if (opacity > 1) {
                opacity = 1;
            }
            disabled.style.transform = 'scaleX(' + scaleX + ')' + 'scaleY(' + scaleY + ')' + 'rotate(' + rotate + 'deg)';
            disabled.style.opacity = '' + opacity;
            wheelOn.style.display = display;
            // blurBg.style.display = display;
            e.preventDefault();
        });

    }
    else {
    }
};


// var testX = document.pageX();
// var testY = document.pageY();
// console.log(testX);
// console.log(testY);


/*Вариант Ильи*/
/*$('#teamContent').on('mousewheel', function (e) {

 var delta = e.deltaY || e.detail || e.wheelDelta;
 if (delta > 0) {
 scale -= 0.05;
 scaleX -= 0.1;
 scaleY -= 0.05;
 }
 // else if (delta < 0 && delta === 0) {
 else {
 scale += 0.05;
 scaleX += 0.1;
 scaleY += 0.05;
 }

 if (scaleX < 0.65) {
 scaleY -= 0.15;
 }

 if (rotate > 1) {
 rotate += 25;
 }
 else {
 rotate += -20;
 }

 if (opacity >= 1) {
 opacity -= 0.1;
 }

 else if (opacity < 0.6) {
 opacity -= 0.175;
 }

 wheelOn.style.transform = 'scaleX(' + scaleX + ')' + 'scaleY(' + scaleY + ')' + 'rotate(' + rotate + 'deg)';
 wheelOn.style.opacity = '' + opacity;

 e.preventDefault();

 });*/




