$(document).ready(function () {
    $(".с-download__text--link").click(function () {
        $(".c-download__block--link").addClass("is-active");
        $(".c-download__icon").addClass("is-active");
    });

    $(".c-download__text--file").click(function () {
    });

    $("#c-download__file").change(function () {
        var file = this.files[0];
        $(".c-download__block--file").addClass("is-active");
        $("#c-download__fileName").text(file.name);
    });


    $(".js-download__cancel").click(function () {
        $(this).parent().removeClass("is-active");
        $(this).siblings(".c-download__input").addClass("is-active").val("");
        $(this).siblings("#c-download__fileName").text("Выберите файл").val("");
        $(this).parent().children(".c-download__file").val('');
        $(".c-download__icon").removeClass("is-active");
    });

    $("#filePull").change(function () {
        var file = this.files[0];
        $("#c-download__fileName").text(file.name);
        $(".c-download__dot").addClass("is-active");
        $(".c-download__text").addClass("is-active");
    })
});


/!*ВАЛИДАЦИЯ*!/

$(function () {
    var form = $("#connectForm");
    var submitButton = $("#c-form__submit");
    var requiredInput = $(".c-form__input--required");

    $.validator.addMethod("phoneLengthValidation",
        function (value, element) {
            console.log(value.replace(/\D+/g, '').length);
            return value.replace(/\D+/g, '').length >= 10;
        });
    $.validator.addMethod("emailCustomValidation",
        function (value, element) {
            return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
        }
    );


    var validatorRules = form.validate({
        rules: {
            name: {
                required: true,
                minlength: 2,
                maxlength: 20
            },
            phone: {
                required: true,
                phoneLengthValidation: true
            },
            email: {
                required: true,
                emailCustomValidation: true,
                email: true,
                maxlength: 30
            },
            position: {
                required: true,
                minlength: 2,
                maxlength: 20
            }
        }
    });
});

$(document).on("click", "#c-form__submit", function () {
    var form = $("#connectForm");
    if (!form.valid()) {
        $('html, body').animate({
            scrollTop: $("#connectForm .c-form__input--required").first().offset().top - 250,
        }, 700);
    }
});


var preloader = {
    init: function () {
        // $(".c-form__button__result").removeClass("is-active");
        $(".c-form__button__text").addClass("is-hide");
        $(".c-form__button__preloader").addClass("is-active");
        $(".c-preloader__icon").addClass("is-active");

    },
    successfull: function () {
        $(".c-form__button__preloader").removeClass("is-active");
        $(".c-preloader__icon").removeClass("is-active");

        $(".c-form__button__text").removeClass("is-hide");

        $(".c-form__button__text").text("Спасибо!");
        // $(".c-form__button__result").addClass("is-active");
    },
    error: function () {
        $(".c-form__button__preloader").removeClass("is-active");
        $(".c-preloader__icon").removeClass("is-active");
        // $(".c-form__button__preloader").removeClass("is-active");
        $(".c-form__button__text").removeClass("is-hide");
        $(".c-form__button__text").text("Попробуйте снова!");
        // $(".c-form__button__result").attr("src", "img/cancel-red.svg")
        // $(".c-form__button__result").addClass("is-active");
    }
};

$(document).on("submit", "#connectForm", function (e) {
    var url = "/script.php",
        form = $(this);

    preloader.init();

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(),
        success: function (data) {
            preloader.successfull();
            form.trigger("reset")
        },
        error: function (xhr) {
            preloader.error();
        }
    });
    e.preventDefault();
});