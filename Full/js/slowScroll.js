/**
 * Created by AvS on 09.02.2017.
 */


$(document).ready(function () {
    $(".js-header__link").on("click", function (e) {
        e.preventDefault();
        var currentBlock = $(this).attr("href"),
            currentBlockOffset = $(currentBlock).offset().top;
        $("html, body").animate({
            scrollTop: currentBlockOffset - 100
        }, 800);
    });

    $(".js-header__link--note").on("click", function (e) {
        e.preventDefault();
        var currentBlock = $(this).attr("href"),
            currentBlockOffset = $(currentBlock).offset().top;
        $("html, body").animate({
            scrollTop: currentBlockOffset
        }, 800);
    });


    var documentWidth = $(window).width();
    console.log(documentWidth);


    if (/iPad/i.test(navigator.userAgent)) {
        $.scrollify.disable();
    }


    if (documentWidth > 768) {
        $(document).on("scroll", function () {
            var headerHeight = $(".l-header").height();
            var teamHeight = $(".l-team").height();
            var documentScroll = $(this).scrollTop();

            if (documentScroll > (headerHeight + 0.1 * teamHeight)) {
                $(function () {
                    setTimeout(function () {
                        $(".l-team__authorBlock__content").addClass("is-hide").fadeOut(600);
                        $("#hideThisText").fadeOut(100);
                    }, 1000)
                })
            }
        })
    }
    if (documentWidth < 768 ) {
        $.scrollify.disable();
    }
});

$(document).on("scroll", function () {
        var headerHeight = $(".l-header").height();
        var teamHeight = $(".l-team").height();
        var videoHeight = $(".l-video").height();
        var whyHeight = $(".l-why").height();
        var portretHeight = $(".l-portret").height();
        var contactHeight = $(".l-contacts").height();
        var documentScroll = $(this).scrollTop();



        if (documentScroll > (headerHeight + teamHeight * 0.9) && $('.c-video__firstVideo').attr('src') == '') {


            $(function () {
                setTimeout(function () {
                    $('.c-video__img').fadeOut(600);
                }, 800);
            });

            $(function () {
                setTimeout(function () {
                    $('.c-video__overLay').fadeIn(600);
                }, 200);
            });
        }

        $('.l-portret__item').each(function (i) {
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();

            if (bottom_of_window > bottom_of_object) {
                $(this).animate({'opacity': '1'}, 500);
            }
        });

        if (documentScroll > 0 && documentScroll < headerHeight) {
            $(".l-header__line").css({
                'transform': 'translate(0px, ' + (documentScroll) * 0.1 + 'px )'
            });

            $(".l-header__line--dotted").css({
                'transform': 'translate(0px, ' + (-documentScroll) * 0.2 + 'px )'
            });
        }

        var forVideo = headerHeight + teamHeight;

        if (documentScroll > forVideo && documentScroll < forVideo + videoHeight) {
            $(".l-video__line").css({
                'transform': 'translate(0px, ' + (-(documentScroll - forVideo)) * 0.3 + 'px )'
            });

            $(".l-video__line--dotted").css({
                'transform': 'translate(0px, ' + (documentScroll - forVideo) * 0.2 + 'px )',
                'opacity': "" + ( 100 / (documentScroll - forVideo)) + ''
            });
        }


        var forWhy = (headerHeight + teamHeight + videoHeight + whyHeight * 0.3);

        if (documentScroll > forWhy && documentScroll < (forWhy + whyHeight + portretHeight)) {
            $(".l-why__line").css({
                'transform': 'translate(0px, ' + (documentScroll - forWhy) * 0.15 + 'px )',
                'opacity': "" + ( forWhy / (documentScroll * 1.125) ) + ''
            });
            $(".l-why__shape").css({
                'transform': 'translate(0px, ' + (-(documentScroll - forWhy)) * 0.3 + 'px )'
            });
        }

        var forPortret = (headerHeight + teamHeight + videoHeight + whyHeight);

        if (documentScroll > forPortret && documentScroll < (forPortret + portretHeight)) {
            $(".l-portret__line").css({
                'transform': 'translate(0px, ' + (-(documentScroll - forPortret) * 0.5) + 'px )'
            });
        }

        var forContact = (headerHeight + teamHeight + videoHeight + whyHeight + portretHeight + contactHeight * 0.4);

        var forForm = (headerHeight + teamHeight + videoHeight + whyHeight + portretHeight + contactHeight * 0.6);

        if (documentScroll > forVideo) {
            $.scrollify.destroy();
        }

        if (documentScroll > forContact && documentScroll < (forContact + contactHeight)) {
            $(".l-contacts__line--second").css({
                'transform': 'translate(0px, ' + (-(documentScroll - forContact) * 0.025) + 'px )'
            });

            $(".l-contacts__line--first").css({
                'transform': 'translate(0px, ' + ((documentScroll - forContact) * 0.25) + 'px )'
            });
        }
        if (documentScroll > forForm && documentScroll < (forForm + contactHeight)) {
            $(".c-form__line--first").css({
                'transform': 'translate(0px, ' + ((documentScroll - forForm) * 0.25) + 'px )'
            });
            $(".l-contacts__line--third").css({
                'transform': 'translate(0px, ' + ((documentScroll - forForm) * 0.25) + 'px )'
            });
        }
    }
);
